from django.db import models

# Create your models here.

class Contenido(models.Model):  #Sera cada una de las columnas de esa tabla los atributos de esa clase

    clave = models.CharField(max_length=64) # Definimos que tenga un tamaño maximo, va a tener maximo 64 caracteres

    valor = models.TextField()