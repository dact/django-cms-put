
from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from.models import Contenido

# Create your views here.

form = '<form action="" method="post" class="form-example">' \
       + '<div class="form-example"><label for="contenido">Contenido: </label>' \
       + '<input type="contenido" name="contenido" id="contenido" required>' \
       + '</div>' \
       + '<div class="form-example">' \
       + '<input type="submit" value="Enviar">' \
       + '</div></form>'

@csrf_exempt
def get_resources(request,recurso):
    if request.method=="PUT":
        valor = request.body.decode()
        c = Contenido(clave=recurso,valor=valor)
        c.save()
    if request.method == "POST":
        valor = request.body.decode().split('=')[1].replace("+", " ")
        c = Contenido(clave=recurso, valor=valor)
        c.save()
    #Método Get
    try:
        contenido = Contenido.objects.get(clave=recurso)
    except Contenido.DoesNotExist:
        return HttpResponse('El recurso pedido no existe' + form)
    return HttpResponse("El recurso pedido es: " + contenido.clave + " Cuyo valor es: " + contenido.valor
                            + " y su identificador es: " + str(contenido.id))
